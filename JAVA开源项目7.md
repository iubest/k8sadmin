## 第一套系统：Jeecg-boot快速开发平台

[参考](https://mp.weixin.qq.com/s?__biz=MzI4Njc5NjM1NQ==&mid=2247497289&idx=1&sn=0cbf895d56721e6b261291ca859a6722&chksm=ebd5c965dca240739d851e0a519061a39c0c4269ee369442d6ee0b2d66a51e0545280e03ef75&mpshare=1&scene=24&srcid=0921QbmRnXVsExBcRGa8TShl&sharer_sharetime=1600660000138&sharer_shareid=407c90840c4caeeaf9680b1dd38c62ba#rd)

前端采用阿里的ant-design-vue，兼容PC端、手机端、Pad端。

#### 开发环境

- 语言：Java 8
- IDE(JAVA)：IDEA / Eclipse安装lombok插件
- IDE(前端)：WebStorm 或者 IDEA
- 依赖管理：Maven
- 数据库：MySQL5.7+ & Oracle 11g & Sqlserver2017
- 缓存：Redis

#### 后端

- 基础框架：Spring Boot 2.1.3.RELEASE
- 持久层框架：Mybatis-plus_3.1.2
- 安全框架：Apache Shiro 1.4.0，Jwt_3.7.0
- 数据库连接池：阿里巴巴Druid 1.1.10
- 缓存框架：redis
- 日志打印：logback
- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。

#### 前端

- Vue 2.6.10,Vuex,Vue Router
- Axios
- ant-design-vue
- webpack,yarn
- vue-cropper - 头像裁剪组件
- @antv/g2 - Alipay AntV 数据可视化图表
- Viser-vue - antv/g2 封装实现
- eslint，@vue/cli 3.2.1
- vue-print-nb - 打印

大屏展示：


统计报表


菜单栏



统计图


流程表


PAD端展示


github地址：

> https://github.com/zhangdaiscott/jeecg-boot

技术文档

- 在线演示 ：http://boot.jeecg.com
- 技术官网：http://www.jeecg.com
- 开发文档：http://doc.jeecg.com

## 第二套系统：renren-fast

renren-fast 是一个轻量级的 Spring Boot 快速开发平台，能快速开发项目并交付【接私活利器】 完善的 XSS 防范及脚本过滤，彻底杜绝 XSS 攻击，实现前后端分离，通过 token 进行数据交互



github地址：

> https://github.com/renrenio/renren-fast-vue

renren-fast-vue

- renren-fast-vue基于vue、element-ui构建开发，实现renren-fast后台管理前端功能，提供一套更优的前端解决方案
- 前后端分离，通过token进行数据交互，可独立部署
- 主题定制，通过scss变量统一一站式定制
- 动态菜单，通过菜单管理统一管理访问路由
- 数据切换，通过mock配置对接口数据／mock模拟数据进行切换
- 发布时，可动态配置CDN静态资源／切换新旧版本
- 演示地址：http://demo.open.renren.io/renren-fast (账号密码：admin/admin)

## 第三套系统：vue-manager-system

这个系统设计的比较优秀，只包含了前端部分，不过功能都比较完善。如下图所示

基于vue + element的后台管理系统解决方案

#### 前言

该方案作为一套多功能的后台框架模板，适用于绝大部分的后台管理系统（Web Management System）开发。基于 vue.js，使用 vue-cli3 脚手架，引用 Element UI 组件库，方便开发快速简洁好看的组件。分离颜色样式，支持手动切换主题色，而且很方便使用自定义主题色。

#### 功能

- Element UI
- 登录/注销
- Dashboard
- 表格
- Tab 选项卡
- 表单
- 图表 📊
- 富文本编辑器
- markdown 编辑器
- 图片拖拽/裁剪上传
- 支持切换主题色 ✨
- 列表拖拽排序
- 权限测试
- 404 / 403
- 三级菜单
- 自定义图标
- 可拖拽弹窗
- 国际化


github地址：

> https://github.com/lin-xin/vue-manage-system

线上地址：

> https://lin-xin.gitee.io/example/work/#/dashboard

整体来看简洁大方，内容完善，功能也可以。

上面是给大家推荐的三种快速开发平台，如有喜欢的拿去就是，都是开源的项目。

## 熬夜收集了 5 个实用的 Java 开源论坛系统！

原创 Guide哥 [JavaGuide](javascript:void(0);) *昨天*

收录于话题

\#「专辑」开源项目推荐

18个

最近有点小忙。但是，由于前几天答应了一位读者自己会推荐一些开源的论坛系统，所以，昨晚就简单地熬了个夜，对比了很多个开源论坛系统之后，总结成了这篇文章。

这篇文章我一共推荐了 5 个论坛类开源项目，除了有 1 个是基于 PHP 开发之外，其他都是基于 Java ,并且大部分都是基于 Spring Boot 这个主流框架来做的。

*欢迎小伙伴们在评论区补充啊！ღ( ´･ᴗ･` )比心*

[参考](https://mp.weixin.qq.com/s?__biz=Mzg2OTA0Njk0OA==&mid=2247492771&idx=1&sn=a8e3fd8a3d123287b5a84303d76cb3d5&chksm=cea1ab68f9d6227e2919b5aa7fce6c1a7a0cf31a7a1468eaa81b420ea86c3cd9cc50e9f8bd09&mpshare=1&scene=24&srcid=0923odg4tV7YN9uYfTZOCpP7&sharer_sharetime=1600856491850&sharer_shareid=407c90840c4caeeaf9680b1dd38c62ba#rd)

## 1. NiterForum

- Github 地址：**https://github.com/yourkevin/NiterForum**[1]
- 官网地址：**https://ld246.com/**[2]
- Star : 0.5k
- 简介：尼特社区-NiterForum-一个论坛程序，几乎具有一个论坛/社区所应该有的全部功能-后端 Springboot/MyBatis/Maven/MySQL-前端 Thymeleaf/Layui-可供初学者，学习、交流使用。
- 技术栈：后端 Springboot + MyBatis + Maven + MySQL 前端 Thymeleaf + Layui
- 推荐等级 ：⭐⭐⭐⭐⭐
- 评价：可以说 NiterForum 提供了一个论坛所能提供的所有功能，功能特性覆盖的非常全面。但这并不是这次推荐他的主要原因。作为本次论坛项目中第一个推荐的项目，NiterForum 的 NB 之处就是：他提供 NiterApp，完美适配了 NiterForum，支持 app 端扫码登录！


## 2. Symphony

- Github 地址：**https://github.com/88250/symphony**[3]
- 官网地址：**https://ld246.com/**[4]
- Star : 0.7k
- 简介：🎶 一款用 Java 实现的现代化社区（论坛/问答/BBS/社交网络/博客）系统平台。
- 技术栈：Latke （作者自研的以 JSON 为主的 Java Web 框架）+**jsoup**[5] + **Jodd**[6]
- 推荐等级 ：⭐⭐⭐⭐
- 评价：讲真，Symphony 是笔者目前见过的论坛项目中功能最齐全的一款（没有之一），满足多维需求：面向内容、面向知识问答、面向用户分享、交友、游戏等。而且 Symphony 风格时尚，充满创新、好玩的特性。交互体验一级棒。这个项目的缺点也很明显，那就是项目使用的技术栈不是主流，比较小众（*不过，作者自研 Java Web 框架的精神还是非常值得赞赏的！*）。


## 3. 码问社区

- Github 地址：**https://github.com/codedrinker/community**[7]
- 官网地址：**http://www.mawen.co/?sort=hot**[8]
- Star : 1.1k
- 简介：开源论坛、问答系统，现有功能提问、回复、通知、最新、最热、消除零回复功能。
- 技术栈：SpringBoot + MyBatis+MySQL/H2+Flyway
- 推荐等级 ：⭐⭐⭐⭐⭐
- 评价：码问社区的作者是阿里巴巴的一位大佬，开源了很多有意思的项目，码问社区就是其中一款，采用 SpringBoot + Vue 等主流技术栈打造，并配有整个开发过程的**视频讲解**[9]。**实战项目首推。**


## 4. MDclub

- Github 地址：**https://github.com/zdhxiong/mdclub**[10]
- 官网地址：**https://community.mdclub.org/**[11]
- Star : 0.5k
- 简介：MDClub 漂亮、轻量且好用，它能让在线讨论变得更加轻松愉悦
- 技术栈：PHP+MySQL
- 推荐等级 ：⭐⭐⭐⭐
- 评价 ：MDclub 是一款简约风格的论坛项目。漂亮、轻量且容易上手。代码实现基于 MDUI 框架，分层分明。网站适配多种终端，从手机、ipad 到大屏显示器，均能自动适配，并且提供根据操作系统的主题，自动切换亮色主题和暗色主题。这个特性真的超赞的~



## 5. 朋也社区

- Github 地址：**https://github.com/tomoya92/pybbs**[12]
- 官网地址：**https://tomoya92.github.io/pybbs/**[13]
- Star : 1.1 k
- 简介：更实用的 Java 开发的社区(论坛)
- 技术栈：Spring-Boot + Mybatis-Plus + MySQL
- 推荐等级 ：⭐⭐⭐⭐
- 评价：朋也社区基于 Java 语言，采用主流的 Java Web 开发框架（SpringBoot）进行开发。个人觉得朋也社区最大的亮点是在设计层面上支持高度的可定制化。要实现这点很不容易，需要有很强的设计能力，并且朋也社区在实现过程对于各种集成的服务支持配置化（可随意开启或关闭）。

