# mall4j商城系统

一个基于spring boot的JAVA开源商城系统，是前后端分离、为生产环境多实例完全准备、数据库为b2b2c商城系统设计、拥有完整下单流程和精美设计的java开源商城系统

https://gitee.com/gz-yami/mall4j


# 什么是JeecgBoot？

今天，要和大家分享一个 Java 项目快速开发脚手架，全新架构前后端分离：SpringBoot 2.x，Ant Design&Vue&，Mybatis，Shiro，JWT。这个项目就叫做——JeecgBoot，近日在Github上很火：


JeecgBoot 是一款基于代码生成器的J2EE快速开发平台，解决Java项目70%的重复工作，让开发更多关注业务逻辑。既能快速提高开发效率，帮助公司节省成本，同时又不失灵活性。JeecgBoot还独创在线开发模式（No代码概念）：在线表单配置（表单设计器）、移动配置能力、工作流配置（在线设计流程）、报表配置能力、在线图表配置、插件能力（可插拔）等等。



JEECG宗旨是：简单功能由Online Coding配置实现（在线配置表单、在线配置报表、在线图表设计、在线设计流程、在线设计表单），复杂功能由代码生成器生成进行手工Merge，既保证了智能又兼顾了灵活; 业务流程采用工作流来实现、扩展出任务接口，供开发编写业务逻辑。






适用项目



Jeecg-Boot快速开发平台，可以应用在任何J2EE项目的开发中，尤其适合企业信息管理系统（MIS）、内部办公系统（OA）、企业资源计划系统（ERP）、客户关系管理系统（CRM）等，其半智能手工Merge的开发方式，可以显著提高开发效率70%以上，极大降低开发成本。





技术架构



开发环境

语言：Java 8
IDE(JAVA)：IDEA / Eclipse安装lombok插件
IDE(前端)：WebStorm 或者 IDEA
依赖管理：Maven
数据库：MySQL5.7+ & Oracle 11g & Sqlserver2017
缓存：Redis


后端

基础框架：Spring Boot 2.1.3.RELEASE
持久层框架：Mybatis-plus_3.1.2
安全框架：Apache Shiro 1.4.0，Jwt_3.7.0
数据库连接池：阿里巴巴Druid 1.1.10
缓存框架：redis
日志打印：logback
其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。


前端

Vue 2.6.10,Vuex,Vue Router
Axios
ant-design-vue
webpack,yarn
vue-cropper - 头像裁剪组件
@antv/g2 - Alipay AntV 数据可视化图表
Viser-vue - antv/g2 封装实现
eslint，@vue/cli 3.2.1
vue-print-nb - 打印



在线试用


登录页面：


项目展示：



Jeecg-Boot已经在Github上标星12.3K，Fork累计4.7K（详情：https://github.com/zhangdaiscott/jeecg-boot）



最后附上相关地址：

在线演示 ： http://boot.jeecg.com

技术官网： http://www.jeecg.com

开发文档： http://doc.jeecg.com

视频教程 ：http://www.jeecg.com/doc/video

常见问题： http://bbs.jeecg.com/forum.php?mod=viewthread&tid=7816&extra=page%3D1

# 学生管理网站

项目为前后分离项目

1、前端(front-end)

基于 vue-admin-template 开发

2、后端(rear-end)

Jdk8
Maven3
MySQL5.7
SpringBoot2
SQLYog

一、功能模块图

图片.png

三、源码获取

前端源码地址

https://github.com/Negen9527/student-manage-system-front

后台源码地址

https://github.com/Negen9527/student-manage-system-rear

四、运行代码

前端
1、git clone https://github.com/Negen9527/student-manage-system-front.git

2、cd student-manage-system-front

3、npm install

4、npm run dev

5、浏览器访问 http://localhost:9528

后台

1、git clone https://github.com/Negen9527/student-manage-system-rear.git

2、导入idea 等待maven依赖下载完成

3、创建数据库 db_stu_man_sys

4、修改项目配置文件中的数据库连接 账号、密码等相关配置

手动创建数据库 db_stu_man_sys，启动项目自动生成表

5、启动项目


不得不佩服 Spring Boot 的生态如此强大，今天我给大家推荐几款 Gitee 上优秀的后台管理系统，小伙伴们再也不用从头到尾撸一个项目了。

**SmartAdmin**

我们开源一套漂亮的代码和一套整洁的代码规范，让大家在这浮躁的代码世界里感受到一股把代码写好的清流！同时又让开发者节省大量的时间，减少加班，快乐工作，热爱生活。SmartAdmin 让你从认识到忘不了，绝对是你最想要的！



项目地址：https://gitee.com/lab1024/smart-admin

## **litemall**

又一个小商场系统，Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端。



项目地址：https://gitee.com/linlinjava/litemall

**Timo**

TIMO后台管理系统，基于SpringBoot2.0 + Spring Data Jpa + Thymeleaf + Shiro 开发的后台管理系统，采用分模块的方式便于开发和维护，支持前后台模块分别部署，目前支持的功能有：权限管理、部门管理、字典管理、日志记录、文件上传、代码生成等，为快速开发后台系统而生的脚手架！

技术选型

- 后端技术：SpringBoot + Spring Data Jpa + Thymeleaf + Shiro + Jwt + EhCache
- 前端技术：Layui + Jquery + zTree + Font-awesome



项目地址：https://gitee.com/aun/Timo

**mall4j**

一个基于spring boot、spring oauth2.0、mybatis、redis的轻量级、前后端分离、防范xss攻击、拥有分布式锁，为生产环境多实例完全准备，数据库为b2b2c设计，拥有完整sku和下单流程的完全开源商城。

项目致力于为中小企业打造一个完整、易于维护的开源的电商系统，采用现阶段流行技术实现。后台管理系统包含商品管理、订单管理、运费模板、规格管理、会员管理、运营管理、内容管理、统计报表、权限管理、设置等模块。



项目地址：https://gitee.com/gz-yami/mall4j

**web-flash**

基于Spring Boot+Vue的后台管理系统,权限管理，字典，配置，定时任务，短信，邮件，根据excel模板导出，cms内容管理，手机端h5，IDEA 代码生成插件。



项目地址：https://gitee.com/enilu/web-flash

**SPTools**

一个基于SpringBoot、JPA、Shiro的后台管理系统，单体架构，依赖少，极易上手，后端开发的福利。最重要的是还附带免费**小程序**以及**微服务版本**，可自行选择。

内置功能

- 组织机构：机构管理、用户管理、角色管理、行政区域。
- 系统监控：系统日志、在线用户，后期会慢慢追加完善。
- 应用管理：任务调度、邮件管理、图片管理、文章管理、打卡任务、数据查询、人工智能，每个模块只需要你稍作修改就可以打造成一个项目了。
- 系统管理：敏捷开发、系统菜单、全局配置、在线代码编辑器，小伙伴们只需要设计好表结构，三秒钟就能撸出一个增删查改的模块。





项目地址：https://gitee.com/52itstyle/SPTools

## **小结**

最后感谢各位撸主贡献出了这么优秀的开源项目，大家可以访问项目支持一下，如果觉得不错可以顺手给个小星星。
















项目地址：https://gitee.com/52itstyle/SPTools
小结

最后感谢各位撸主贡献出了这么优秀的开源项目，大家可以访问项目支持一下，如果觉得不错可以顺手给个小星星。
