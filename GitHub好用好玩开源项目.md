## GitHub 好用好玩值得收藏的开源项目集合~

[程序员书单](javascript:void(0);) *3天前*

以下文章来源于是Kerwin啊 ，作者Kerwin_

> ❝
>
> **「这是我许久以来从各处发现的极佳开源项目，希望分享给大家~ 如果帮到你了，给我个赞啦」**
>
> ❞

## 编程语言类

### ❤️learn-go-with-tests（通过单元测试学Go）

GitHub地址：https://github.com/quii/learn-go-with-tests

通过单元测试学习 Go 语言。下载仓库源码后，进入对应目录。每一个小文件夹就是一个对应的 Go 项目，在里面`go test`即可运行单元测试。由于是测试驱动开发，所以需要在你改动代码之后跑通单元测试才算学会通过。每一个对应的文件夹都有相应 Markdown 文字教程，比较浅显易懂。而且还有中文版：https://studygolang.gitbook.io/learn-go-with-tests

### ❤️500LineorLess_CN（500行Python代码能做什么）

GitHub地址：https://github.com/HT524/500LineorLess_CN

500 line or less 中文翻译计划。

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjiaujXrKLDnNibo0Knnme7iaVx7smqiaOMXnCZg6msCy33lAhPz9cU6rsuA/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### ❤️awesome-leetcode(各大 IT 公司的算法面试题)

GitHub地址：https://github.com/Blankj/awesome-java-leetcode

👑 LeetCode of algorithms with java solution(updating).

### ❤️BrowserQuest（JavaScript多人在线游戏）

GitHub地址：https://github.com/mozilla/BrowserQuest

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjBib8icHStqVDR2N6E1vibNpEKzDGIXKpV7O7ZZ4nQ8bGCGOAkxV62fPsg/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

## 前端类

### ❤️fe-interview（每天四道前端面试题）

GitHub地址：https://github.com/haizlin/fe-interview

万人Star

每天早上 4 个基础前端面试题，助你在前端面试中‘所向披靡，无人能挡

### ❤️vuepress（ Vue 官方出品的静态网站生成器 ）

演示地址：https://vuepress.vuejs.org/

GitHub地址：https://github.com/vuejs/vuepress

Vue 官方出品的静态网站生成器。大家的个人博客是不是要折腾一番了？官方中文文档

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsj11kXMQtrPVRdNnibfNKE56vgoOfibsQWjl6bDoH41t5KmnTQse9cO57A/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### Vue-Form-Making（Vue拖拽生成器）

基于Vue的可视化表单设计器，让表单开发简单而高效。

GitHub链接：https://github.com/GavinZhuLei/vue-form-making

码云链接：https://gitee.com/gavinzhulei/vue-form-making/

直接体验网址：http://form.xiaoyaoji.cn/#/zh-CN/

效果图：

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjBqc8FUwOcibSGb2tq6Rmc3ibGF0amyAvnuJiccnq6rxsl3S4fDDAZWEhg/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### mpvue（基于Vue的微信小程序框架）

演示地址：http://mpvue.com/

GitHub地址：https://github.com/Meituan-Dianping/mpvue

小程序的前端框架。框架基于 Vue.js 核心，修改了 Vue.js 的 runtime 和 compiler 实现。使其可以运行在小程序环境中，为小程序开发引入了整套 Vue.js 开发体验。5 分钟上手视频

- 彻底的组件化开发能力：提高代码复用性
- 完整的 Vue.js 开发体验
- 方便的 Vuex 数据管理方案：方便构建复杂应用
- 快捷的 webpack 构建机制：自定义构建策略、开发阶段 hotReload
- 支持使用 npm 外部依赖
- 使用 Vue.js 命令行工具 vue-cli 快速初始化项目
- H5 代码转换编译成小程序目标代码的能力

### 30-seconds-of-code（30s代码理解）

GitHub地址：https://github.com/30-seconds/30-seconds-of-code

精选可以在 30秒 或更短的时间内理解的实用 JavaScript 代码片段集合

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsj2F8dWlr9Q3PvjwPhtfcAIa4UuzxMZUNzQeWSG6RRlicTapuZP5AaegQ/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### 33-js-concepts（JavaScript 33个概念）

GitHub地址：https://github.com/stephentian/33-js-concepts

📜 每个 JavaScript 工程师都应懂的33个概念

### hotkeys(JavaScript 键盘监听工具)

一个强健的 Javascript 库用于捕获键盘输入和输入的组合键。它没有依赖，压缩只有 3kb 左右。

演示地址：https://wangchujiang.com/hotkeys/

GitHub地址：https://github.com/jaywcjlove/hotkeys

## Google插件

### ❤️simpread（简悦）

GitHub地址：https://github.com/Kenshin/simpread

简悦 ( SimpRead ) - 让你瞬间进入沉浸式阅读的扩展 http://ksria.com/simpread，而且可以一键导出各种格式及平台 谷歌插件好评度：5星 安装人数10W+

### ❤️FeHelper（谷歌插件-前端助手）

GitHub地址：https://github.com/zxlie/FeHelper

Google插件开源项目

### chrome-plugin-demo（Google插件开发说明）

GitHub地址：https://github.com/sxei/chrome-plugin-demo

《Chrome插件开发全攻略》配套完整Demo

## 实用工具

### ❤️owncloud/core（私有云服务，搭建本地云盘）

GitHub地址：https://github.com/owncloud/core

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjhKmxNkjmaHCniac9bj98qk7KJIeuGXVFEdaX0j7FCVg1hjrUFKAbicjw/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### ❤️glances 系统情况工具

GitHub地址：https://github.com/nicolargo/glances

安装：pip install glances

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsj4NwjkJX1g9aicNib8ibpY6WEgIknaPEUzUibE9Kr125c1B8aia8MxBq4lSQ/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### ❤️cmder（Windows控制台）

GitHub地址：https://github.com/cmderdev/cmder

官方地址：https://cmder.net/

Windows 控制台模拟器，运行效果如下：

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjkI1bfNQkV5C2JicApic4NvClZyKaXA7CwPSwXOemeUR8CMlJnBm7QmQg/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### PPRows(Mac代码行数统计)

GitHub地址：https://github.com/jkpang/PPRows

It Can Calculate how many lines of code you write on Mac ; 在Mac上计算你写了多少行代码

### RAP（接口文档管理工具）

GitHub地址：https://github.com/thx/RAP

Web接口管理工具，开源免费，接口自动化，MOCK数据自动生成，自动化测试，企业级管理。阿里妈妈MUX团队出品！阿里巴巴都在用！1000+公司的选择！RAP2已发布请移步至https://github.com/thx/rap2-delos rap2.taobao.org

在线演示：http://rap2.taobao.org/account/login

### lx-music-desktop（全网音乐播放器）

GitHub地址：https://github.com/lyswhut/lx-music-desktop

### wxapp-market（小程序营销组件）

GitHub地址：https://github.com/o2team/wxapp-market

小程序营销组件，使用简单、方式齐全。包含示例代码，玩法多样

- 大转盘
- 刮刮乐
- 老虎机
- 水果机
- ...

### SwitchHosts(Windows切换Host工具)

GitHub地址：https://github.com/oldj/SwitchHosts

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjARRBpXhXfZ0PIF6cibibicEEDV92mL0eGqpKRJ5b8IoVGYNSx5L1J4urA/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

## 书籍源码分享类

### nginx-book(Nginx书籍)

GitHub地址：https://github.com/taobao/nginx-book

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjYhpgiclia5NorPYneAiaL58odlz6pCEibN0W00wmPgE0tQNibH03gU5RelA/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### redis-3.0-annotated（redis 源码注释C语言）

GitHub地址：https://github.com/huangz1990/redis-3.0-annotated

带有详细注释的 Redis 3.0 代码（annotated Redis 3.0 source code）。

### vjtools（唯品会的 Java 技术干货分享）

GitHub地址：https://github.com/vipshop/vjtools

主力于Java的唯品会，关于Java的一些小家底。

### reading-code-of-nginx-1.9.2（Nginx源码阅读）

GitHub地址：https://github.com/y123456yz/reading-code-of-nginx-1.9.2

nginx-1.9.2 源码通读分析注释，带详尽函数中文分析注释以及相关函数流程调用注释

## 特别好玩的东西

### ❤️makegirlsmoe_web（二次元捏脸）

GitHub地址：https://github.com/makegirlsmoe/makegirlsmoe_web

在线尝试：https://make.girls.moe/#/

动漫角色图片生成工具。支持：选择发色、发型、眼睛、皮肤、微笑、风格等等特征生成二次元图片。自定义生成可爱的二次元头像，二次元界福音。

### ❤️emoji(MarkDown表情库)

GitHub地址：https://github.com/gee1k/emoji

演示地址：https://emoji.svend.cc/

Demo：💚💚💚💚💚💚💚💚💚💚💚💚💚

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjRLdyaHT8mKLMMqrsicJbf5rsQQafloUwibBv18meNnDV0E0sE6Es3KFA/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### ❤️vscode-leetcode（LeeCode刷题神器）

直接在vscode里搜VS Code for LeetCode就好啦

这是一个可以让用户在 VS Code 编辑器中，练习 LeetCode 习题的插件。支持：查看高票解答、提交答案、测试答案等。提高了刷题效率，助你在校招、社招中杀出重围。上班摸鱼刷题利器

### ❤️chrome-music-lab（ Chrome 音乐实验室 ）

Chrome 音乐实验室是一个网站，让学习音乐变得更加简单、好玩。完全基于Web端，国内可直接访问、老少皆宜、支持多种乐器，圆你一个音乐梦

演示地址：http://musiclab.chromeexperiments.com/

GitHub地址：https://github.com/googlecreativelab/chrome-music-lab

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjHDnNdHSrPSLbZiaVdAWmV6BAkjLlkgtnLutkT7hic6VQGkENKmib7cVoQ/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1))

### ❤️AutoPiano（HTML5自由钢琴）

GitHub地址：https://github.com/WarpPrism/AutoPiano

演示地址：http://www.autopiano.cn/

自由钢琴（AutoPiano）是利用 HTML5 技术开发的在线钢琴应用。在学习工作之余可以享受钢琴、音乐的美好，支持钢琴曲的自动播放功能、按键提示。让学习钢琴变得简单，谁都可以练成‘钢琴手’

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjRibd72ZdgdrCpRMKxdzyPafDO3gFJw7tnwhMLDIcqxXxDV0qsdicCXGQ/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### awescnb（博客园自定义皮肤）

码云地址：https://gitee.com/guangzan/awescnb

可快速构建博客园皮肤样式

效果图：

![img](https://mmbiz.qpic.cn/mmbiz/hbTNOSuicwltSGWVXNkroF8icY8GCOYFsjibwcnSutPibpKAPxYfhj7PiciaGCPkotyr41WDzzUnloic76uyiaSb8E0pfA/640?wx_fmt=other&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

## GitHub访问慢的解决方案

```
# 本地增加GitHub相关Host
192.30.253.112 github.com
74.125.237.1 dl-ssl.google.com
173.194.127.200 groups.google.com
192.30.252.131 github.com
185.31.16.185 github.global.ssl.fastly.net
74.125.128.95 ajax.googleapis.com
204.232.175.78 documentcloud.github.com
207.97.227.239 github.com
204.232.175.94 gist.github.com
107.21.116.220 help.github.com
207.97.227.252 nodeload.github.com
199.27.76.130 raw.github.com
107.22.3.110 status.github.com
204.232.175.78 training.github.com
207.97.227.243 www.github.com
192.30.253.118 gist.github.com
192.30.253.119 gist.github.com
复制代码
```

## GitHub访问慢的终极解决方案

**「new-pac项目（XX上网）」**

GitHub地址：https://github.com/Alvin9999/new-pac

每隔一段时间会推送免费的账号，我个人使用的就是上述的工具，因为不常用，所以刚好可以满足我的需求

## 最后

感谢GitHub开发者的奉献精神，更多好玩的请关注我吧~