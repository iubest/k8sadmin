Guide 哥注：下面的商城系统大多比较复杂比如 mall ,如果没有 Java 基础和 Spring Boot 都还没有摸熟的话不推荐过度研究下面几个项目或者使用这些项目当作毕业设计。

**”**

1. **mall** ：mall 项目是一套电商系统，包括前台商城系统及后台管理系统，基于 SpringBoot+MyBatis 实现。
2. **mall-swarm** : mall-swarm 是一套微服务商城系统，采用了 Spring Cloud Greenwich、Spring Boot 2、MyBatis、Docker、Elasticsearch 等核心技术，同时提供了基于 Vue 的管理后台方便快速搭建系统。
3. **onemall** ：mall 商城，基于微服务的思想，构建在 B2C 电商场景下的项目实战。核心技术栈，是 Spring Boot + Dubbo 。未来，会重构成 Spring Cloud Alibaba 。
4. **litemall** ：又一个小商城。litemall = Spring Boot 后端 + Vue 管理员前端 + 微信小程序用户前端 + Vue 用户移动端。

**博客/论团/其他**

**“**

Guide 哥注：下面这几个项目都是非常适合 Spring Boot 初学者学习的，下面的大部分项目的总体代码架构我都看过，个人觉得还算不错，不会误导没有实际做过项目的老哥，特别是前两个项目 vhr 和 favorites-web 。

**”**

1. **vhr** ：微人事是一个前后端分离的人力资源管理系统，项目采用 SpringBoot+Vue 开发。
2. **favorites-web** :云收藏 Spring Boot 2.X 开源项目。云收藏是一个使用 Spring Boot 构建的开源网站，可以让用户在线随时随地收藏的一个网站，在网站上分类整理收藏的网站或者文章。
3. **VBlog** ：V 部落，Vue+SpringBoot 实现的多用户博客管理平台!
4. **My-Blog** ：My Blog 是由 SpringBoot + Mybatis + Thymeleaf 等技术实现的 Java 博客系统，页面美观、功能齐全、部署简单及完善的代码，一定会给使用者无与伦比的体验。
5. **community** ：开源论坛、问答系统，现有功能提问、回复、通知、最新、最热、消除零回复功能。功能持续更新中…… 技术栈 Spring、Spring Boot、MyBatis、MySQL/H2、Bootstrap。

**权限管理系统**

**“**

Guide 哥注：权限管理系统在企业级的项目中一般都是非常重要的，如果你需求去实际了解一个不错的权限系统是如何设计的话，推荐你可以参考下面这些开源项目。

**”**

1. **Spring-Cloud-Admin** ：Cloud-Admin 是国内首个基于 Spring Cloud 微服务化开发平台，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关 API 管理等多个模块，支持多业务系统并行开发，可以作为后端服务的开发脚手架。代码简洁，架构清晰，适合学习和直接项目中使用。核心技术采用 Spring Boot2 以及 Spring Cloud Gateway 相关核心组件，前端采用 vue-element-admin 组件。
2. **pig**：（gitee）基于 Spring Boot 2.2、 Spring Cloud Hoxton & Alibaba、 OAuth2 的 RBAC 权限管理系统。
3. **FEBS-Shiro** ：Spring Boot 2.1.3，Shiro1.4.0 & Layui 2.5.4 权限管理系统。
4. **eladmin** : 项目基于 Spring Boot 2.1.0 、 Jpa、 Spring Security、redis、Vue 的前后端分离的后台管理系统，项目采用分模块开发方式， 权限控制采用 RBAC，支持数据字典与数据权限管理，支持一键生成前后端代码，支持动态路由。

 **JeeSite** 

**项目名称：**JeeSite

**项目所用技术栈：**Spring、Spring MVC、MyBatis、Apache Shiro、Bootstrap UI等

**项目简介：**JeeSite 是一个 Java EE 企业级快速开发平台，基于经典技术组合（Spring Boot、Spring MVC、Apache Shiro、MyBatis、Beetl、Bootstrap、AdminLTE）采用经典开发模式，让初学者能够更快的入门并投入到团队开发中去。在线代码生成功能，包括核心模块如：组织机构、角色用户、菜单及按钮授权、数据权限、系统参数、内容管理、工作流等。采用松耦合设计；界面无刷新，一键换肤；众多账号安全设置，密码策略；在线定时任务配置；支持集群，支持SAAS；支持多数据源；支持微服务。


![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205231_76365cd9_5368921.jpeg "3f3f6f658414085a295596d8aff1b07e.jpg")
**项目源码：**https://github.com/thinkgem/jeesite

 **XMall商城** 

**项目名称：**XMall商城

**项目所用技术栈：**SSM、Elasticsearch、Redis、MySQL、ActiveMQ、Shiro、Dubbo、Zookeeper、Vue.js 等

**项目简介：**XMall是一个基于SOA架构的分布式电商购物商城，并且前后端分离。包括如下几大模块：

- 后台管理系统：管理商品、订单、类目、商品规格属性、用户、权限、系统统计、系统日志以及前台内容等功能
- 前台系统：用户可以在前台系统中进行注册、登录、浏览商品、首页、下单等操作
- 会员系统：用户可以在该系统中查询已下的订单、管理订单、我的优惠券等信息
- 订单系统：提供下单、查询订单、修改订单状态、定时处理订单
- 搜索系统：提供商品的搜索功能
- 单点登录系统：为多个系统之间提供用户登录凭证以及查询登录用户的信息

是初学者练手的不错的项目。


![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205331_36d7e762_5368921.jpeg "8497ca5ad216406f0de68d1c9c4c5b7f.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205346_81194881_5368921.jpeg "db59d6740a1233b25e7f5bdff7a0cb1e.jpg")
**项目源码地址：**https://github.com/Exrick/xmall

 **Cloud-Platform** 

**项目名称：**Cloud-Platform

**项目所用技术栈：**Spring Boot、Spring Cloud全家桶、Vue.js等

**项目简介：**Cloud-Platform是国内首个基于Spring Cloud的微服务开发平台，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关API 管理等多个模块，支持多业务系统并行开发，可以作为后端服务的开发脚手架。代码简洁，架构清晰，适合学习和直接项目中使用。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205413_2728c175_5368921.jpeg "e3842fadff038dabe7fd4e9986718c6e.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205425_187e6b50_5368921.jpeg "fd851b067976e6ab6d2235ccf4c7c69d.jpg")

**项目源码：**https://gitee.com/geek_qi/cloud-platform

 **Piggy Metrics** 

**项目名称：**Piggy Metrics

**项目所用技术栈：**Spring Boot、Spring Cloud、Docker等

**项目简介：**Piggy Metrics是一个基于Spring Boot, Spring Cloud和Docker等技术实现的微服务脚手架，项目包含了：API网关、服务发现、负载均衡、熔断机制、配置中心、监控服务、认证服务等几大微服务项目基本模块，对学习和练手微服务项目是不错的选择。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205441_37574756_5368921.jpeg "be66f5f63d5500a53570ada20bb4fded.jpg")

**项目源码：**https://github.com/sqshq/piggymetrics

 **mall电商** 

**项目名称：**mall

**项目所用技术栈：**Spring Boot、Spring Security、MyBatis、Elasticsearch、RabbitMq、Redis、MongoDb、Docker 、Vue.js等

**项目简介：**mall项目是一套电商系统，包括前台商城系统及后台管理系统，基于SpringBoot+MyBatis实现，采用Docker容器化部署。前台商城系统包含首页门户、商品推荐、商品搜索、商品展示、购物车、订单流程、会员中心、客户服务、帮助中心等模块。后台管理系统包含商品管理、订单管理、会员管理、促销管理、运营管理、内容管理、统计报表、财务管理、权限管理、设置等模块。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205507_fa06fb8c_5368921.jpeg "8c4214d0a7d8da25f7cc2243e2f84963.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205518_a471e237_5368921.jpeg "da1eaede74e96e394d0aeb98fe964a53.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/205531_1ab6b880_5368921.jpeg "c6da14326705dcb1851bbdf4eafb1974.jpg")

**项目源码：**https://github.com/macrozheng/mall