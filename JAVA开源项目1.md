### spring-boot-examples


•star 数 14821•项目地址：https://github.com/ityouknow/spring-boot-examples

这个项目中整合了 Spring Boot 使用的各种示例，以最简单、最实用为标准，此开源项目中的每个示例都以最小依赖，最简单为标准，帮助初学者快速掌握 Spring Boot 各组件的使用。基本上涉及到了 Spring Boot 使用的方方面面。



### 微人事


•star 数 3333•项目地址：https://github.com/lenve/vhr

微人事是一个前后端分离的人力资源管理系统，项目采用 SpringBoot + Vue 开发。项目打通了前后端，并且提供了非常详尽的文档，从 Spring Boot 接口设计到前端 Vue 的开发思路，作者全部都记录在项目的 wiki 中，是不可多得的 Java 全栈学习资料。






### mall


•star 数 12668•项目地址：https://github.com/macrozheng/mall

mall 项目是一套电商系统，包括前台商城系统及后台管理系统，基于 Spring Boot + MyBatis 实现。前台商城系统包含首页门户、商品推荐、商品搜索、商品展示、购物车、订单流程、会员中心、客户服务、帮助中心等模块。后台管理系统包含商品管理、订单管理、会员管理、促销管理、运营管理、内容管理、统计报表、财务管理、权限管理、设置等模块。




### spring-boot-pay


•star 数 2931•项目地址：https://gitee.com/52itstyle/spring-boot-pay

这是一个支付案例，提供了包括支付宝、微信、银联在内的详细支付代码案例，对于有支付需求的小伙伴来说，这个项目再合适不过了。



### V 部落


•star 数 1060•项目地址：https://github.com/lenve/VBlog

V部落是一个多用户博客管理平台，采用 Vue + SpringBoot + ElementUI 开发。**这个项目最大的优势是简单，属于功能完整但是又非常简单的那种，非常非常适合初学者。**




### springboot-plus


•star 数 2546•项目地址：https://gitee.com/xiandafu/springboot-plus

一个基于SpringBoot 2 的管理后台系统,包含了用户管理，组织机构管理，角色管理，功能点管理，菜单管理，权限分配，数据权限分配，代码生成等功能 相比其他开源的后台系统，SpringBoot-Plus 具有一定的复杂度。系统基于Spring Boot2.1技术，前端采用了Layui2.4。数据库以MySQL/Oracle/Postgres/SQLServer为实例，理论上是跨数据库平台。




### litemall


•star 数 6436•项目地址：https://github.com/linlinjava/litemall

一个商城项目，包括Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端，功能包括、分类列表、分类详情、品牌列表、品牌详情、新品首发、人气推荐、优惠券列表、优惠券选择、团购（团购业务有待完善）、搜索、商品详情、商品评价、商品分享、购物车、下单、订单列表、订单详情、地址、收藏、足迹、意见反馈以及客服；管理平台功能包括会员管理、商城管理、商品管理、推广管理、系统管理、配置管理、统计报表等。

**1**

**Alink**

https://github.com/alibaba/Alink Star 1695

Alink 是阿里巴巴计算平台事业部PAI团队从 2017 年开始基于实时计算引擎 Flink 研发的新一代机器学习算法平台，提供丰富的算法组件库和便捷的操作框架，开发者可以一键搭建覆盖数据处理、特征工程、模型训练、模型预测的算法模型开发全流程。Alink 已被广泛运用在阿里巴巴搜索、推荐、广告等多个核心实时在线业务中。

**2**

**halo**

https://github.com/halo-dev/halo Star 11617



这是一款现代化的、轻快，简洁，功能强大，使用Java开发的博客系统。

**3**

**Spring-boot-demo**

https://github.com/xkcoding/spring-boot-demo Star 8866

Spring boot demo 是一个用来深度学习并实战 Spring boot 的项目，目前总共包含 63 个集成demo，已经完成 52 个。 该项目已成功集成 actuator(监控)、admin(可视化监控)、logback(日志)、aopLog(通过AOP记录web请求日志)、统一异常处理(json级别和页面级别)、freemarker(模板引擎)、thymeleaf(模板引擎)、Beetl(模板引擎)、Enjoy(模板引擎)、JdbcTemplate(通用JDBC操作数据库)、JPA(强大的ORM框架)等

**4**

**BigData-Notes**

https://github.com/heibaiying/BigData-Notes Star 4341

大数据入门教程，该教程介绍了大数据常用技术栈的基础和核心知识。内容涵盖：Hadoop、Spark、Storm、HBase、Hive、ZooKeeper、Kafka 等。

**5**

**vhr**

https://github.com/lenve/vhr Star 12987



微人事是一个前后端分离的人力资源管理系统，项目采用SpringBoot+Vue开发。

**6**

**flink-learning**

https://github.com/zhisheng17/flink-learning Star 4280

该项目含 Flink 入门、概念、原理、实战、性能调优、源码解析等内容。涉及 Flink Connector、Metrics、Library、DataStream API、Table API & SQL 等内容的学习案例，还有 Flink 落地应用的大型项目案例分享。

**7**

**LeetCodeAnimation**

https://github.com/MisterBooo/LeetCodeAnimationStar 47297

该项目将LeetCode 上所有的题目都用动画的形式演示出来，创建者计划用3到4年的时间完成，目前已有的算法还是比较少的

**8**

**jeecg-boot**

https://github.com/zhangdaiscott/jeecg-boot Star 9044

一款基于代码生成器的JAVA快速开发平台，开源界“小普元”超越传统商业企业级开发平台！采用前后端分离架构：SpringBoot 2.x，Ant Design&Vue，Mybatis-plus，Shiro，JWT。强大的代码生成器让前后端代码一键生成，无需写任何代码！号称可以帮助Java项目解决70%的重复工作，让开发更多关注业务逻辑。

**9**

**advanced-java**

https://github.com/doocs/advanced-java Star 36863

本系列知识出自中华石杉，可以作为互联网 Java 工程师进阶知识完全扫盲。学习本系列知识之前，如果你完全没接触过 MQ、ES、Redis、Dubbo、Hystrix 等，那么我建议你可以先在网上搜一下每一块知识的快速入门，跟着入门 Demo 玩一下，然后再开始每一块知识的学习，这样效果更好

**10**

**mall**

https://github.com/macrozheng/mall Star 27700



mall项目是一套电商系统，包括前台商城系统及后台管理系统，基于SpringBoot+MyBatis实现。前台商城系统包含首页门户、商品推荐、商品搜索、商品展示、购物车、订单流程、会员中心、客户服务、帮助中心等模块。后台管理系统包含商品管理、订单管理、会员管理、促销管理、运营管理、内容管理、统计报表、财务管理、权限管理、设置等模块。

**1**

**12306**

https://github.com/testerSunshine/12306 Star 25912

12306这个项目基本上是紧随着12306网站的功能更新而更新的，支持12306的所有基本功能，作者的设计思路也是很简单：

!

**2**

**architect-awesome**

https://github.com/xingshaocheng/architect-awesomeStar 41507

一套非常全面的后端架构师技术图谱，从数据结构与算法着手，带你学习后端技术的方方面面。目前 GitHub 上已有 41507 Star

**3**

**chinese-poetry**

https://github.com/chinese-poetry/chinese-poetryStar 25169

这是一个程序员自编的中华古诗词数据库，该诗词数据库包含5.5万首唐诗、26万首宋诗和2.1万首宋词。唐宋两朝近1.4万古诗人，和两宋时期1500位词人，数据均来源于互联网。

**4**

**javascript-algorithms**

https://github.com/trekhleb/javascript-algorithmsStar 59312

javascript-algorithms包含了多种基于JavaScript的算法与数据结构，每种算法和数据结构都有自己的README并提供相关说明以及进一步阅读和YouTube视频。

**5**

**Alink**

https://github.com/alibaba/AlinkStar 1695

Alink是阿里巴巴计算平台事业部PAI团队从2017年开始基于实时计算引擎Flink研发的新一代机器学习算法平台，提供丰富的算法组件库和便捷的操作框架，开发者可以一键搭建覆盖数据处理、特征工程、模型训练、模型预测的算法模型开发全流程。Alink已被广泛运用在阿里巴巴搜索、推荐、广告等多个核心实时在线业务中。

**6**

**halo**

https://github.com/halo-dev/haloStar 11617



这是一款现代化的、轻快，简洁，功能强大，使用Java开发的博客系统。

**7**

**spring-boot-demo**

https://github.com/xkcoding/spring-boot-demoStar 8866

Spring boot demo是一个用来深度学习并实战Spring boot的项目，目前总共包含63个集成demo，已经完成52个。 该项目已成功集成actuator(监控)、admin(可视化监控)、logback(日志)、aopLog(通过AOP记录web请求日志)、统一异常处理(json级别和页面级别)、freemarker(模板引擎)、thymeleaf(模板引擎)、Beetl(模板引擎)、Enjoy(模板引擎)、JdbcTemplate(通用JDBC操作数据库)、JPA(强大的ORM框架)等

**8**

**flowy**

https://github.com/alyssaxuu/flowyStar 5121



这是一个JavaScript库，能够帮助你轻松创建漂亮的流程图

**9**

**BigData-Notes**

https://github.com/heibaiying/BigData-NotesStar 4341

这是一个大数据入门教程，该教程介绍了大数据常用技术栈的基础和核心知识。内容涵盖：Hadoop、Spark、Storm、HBase、Hive、ZooKeeper、Kafka 等。

**10**

**flutter**

https://github.com/flutter/flutterStar 82830

Flutter是一种新型的方式，用于创建高性能、跨平台的移动应用。由Google的工程师团队打造。Flutter针对当下以及未来的移动设备进行优化，专注于Android and iOS低延迟的输入和高帧率。