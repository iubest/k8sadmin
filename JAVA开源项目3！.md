# RuoYi 

这里只有一个项目。

但是这个项目，很全面！


## bootstrap版本开源地址：

https://gitee.com/y_project/RuoYi/

bootstrap版本：

演示地址：http://ruoyi.vip

文档地址：http://doc.ruoyi.vip

## vue前后端分离版本开源地址：

https://gitee.com/y_project/RuoYi-Vue

vue前后端分离版本：

演示地址：http://vue.ruoyi.vip

文档地址：http://doc.ruoyi.vip

微服务版本：

https://gitee.com/y_project/RuoYi-Cloud

采用前后端分离的模式，微服务版本前端(基于 RuoYi-Vue)。

后端采用Spring Boot、Spring Cloud & Alibaba。

注册中心、配置中心选型Nacos，权限认证使用Redis。

流量控制框架选型Sentinel。