## 运维平台

## CMDB

### 开源 cmdb 系统

github 仓库
后端：https://github.com/open-cmdb/cmdb

前端：https://github.com/open-cmdb/cmdb-web

基本功能有：热添加删除表、自定义字段类型，方便增删改查的前端界面，强大的搜索查找能力（后端使用elasticsearch存储数据 ） 可以配合 kibana 使用，查看数据的删除修改记录、历史版本等，还带有表级权限管理，开放所有 API。


https://github.com/welliamcao/OpsManage

https://gitee.com/lwr_dgas/AnsibleUI


### spug
https://gitee.com/iubest/spug.git
Spug是面向中小型企业设计的轻量级无Agent的自动化运维平台，整合了主机管理、主机批量执行、主机在线终端、应用发布部署、在线任务计划、配置中心、监控、报警等一系列功能。



## 博客

https://github.com/myminwang/myblog

https://github.com/newpanjing/myblog

https://gitee.com/iubest/HelloDjango-blog-tutorial




### DjangoBlog 网站


https://github.com/liangliangyy/DjangoBlog

基于python3.8和Django3.0的博客。

Build Status codecov Requirements Status  license

主要功能：
文章，页面，分类目录，标签的添加，删除，编辑等。文章及页面支持Markdown，支持代码高亮。
支持文章全文搜索。
完整的评论功能，包括发表回复评论，以及评论的邮件提醒，支持Markdown。
侧边栏功能，最新文章，最多阅读，标签云等。
支持Oauth登陆，现已有Google,GitHub,facebook,微博,QQ登录。
支持Memcache缓存，支持缓存自动刷新。
简单的SEO功能，新建文章等会自动通知Google和百度。
集成了简单的图床功能。
集成django-compressor，自动压缩css，js。
网站异常邮件提醒，若有未捕捉到的异常会自动发送提醒邮件。
集成了微信公众号功能，现在可以使用微信公众号来管理你的vps了。


## 其他

### 配套代码
https://gitee.com/iubest/djweb-code

https://gitee.com/geektime-geekbang/django

https://gitee.com/iubest/music-web

https://gitee.com/iubest/DjangoPracticeProject

https://gitee.com/iubest/Django3-Web

### 腾讯蓝鲸


BlueKing Community
- [BK-BCS](https://github.com/Tencent/bk-bcs)：蓝鲸容器管理平台是以容器技术为基础，为微服务业务提供编排管理的基础服务平台。
- [BK-BCS-SaaS](https://github.com/Tencent/bk-bcs-saas)：蓝鲸容器管理平台SaaS基于原生Kubernetes和Mesos自研的两种模式，提供给用户高度可扩展、灵活易用的容器产品服务。
- [BK-CMDB](https://github.com/Tencent/bk-cmdb)：蓝鲸配置平台（蓝鲸CMDB）是一个面向资产及应用的企业级配置管理平台。
- [BK-PaaS](https://github.com/Tencent/bk-PaaS)：蓝鲸PaaS平台是一个开放式的开发平台，让开发者可以方便快捷地创建、开发、部署和管理SaaS应用。
- [BK-SOPS](https://github.com/Tencent/bk-sops)：蓝鲸标准运维（SOPS）是通过可视化的图形界面进行任务流程编排和执行的系统，是蓝鲸体系中一款轻量级的调度编排类SaaS产品。

 **文档中心** 

https://bk.tencent.com/docs/