## 再次安利 5 个接私活必备的 Java 开源项目！

[GitHubDaily](javascript:void(0);) *昨天*

以下文章来源于JavaGuide ，作者Guide哥

[![JavaGuide](http://wx.qlogo.cn/mmhead/Q3auHgzwzM6j6ibiaOyeuibicQh9mQua4cSLP2fax8vNW25Wm8ic6kUQFlQ/0)**JavaGuide**专注Java后端学习和大厂面试的公众号！开源项目—JavaGuide （56k+Star）作者运营维护。](https://mp.weixin.qq.com/s?__biz=MzAxOTcxNTIwNQ==&mid=2457922578&idx=4&sn=abf91d878f373487dcff2e1524b38493&chksm=8cb689f8bbc100eee0807a7e23f9c8c7eaa646fa4f63886dff3549be7492da64fbdc3b940bc5&mpshare=1&scene=24&srcid=&sharer_sharetime=1590982903848&sharer_shareid=407c90840c4caeeaf9680b1dd38c62ba&key=d3e5cf3db18e3c2542968386a26600e3a3e0c56751da0440f397ce13aafb57d9639fa71088ff17f83287df14ac4b5a15502d02b818f2eff024a716bb9bff311f536395835be06c11486512e4e2a08090&ascene=14&uin=MTIwMjI3NTkwNQ%3D%3D&devicetype=Windows+10+x64&version=6209007b&lang=zh_CN&exportkey=A%2FzckzkRzCXahgheGPsEbmQ%3D&pass_ticket=BZ5fPjk572yMEjGWm5bJ0ERCE%2Fjz%2FQ3HARPvXkvYvYg%2BOPLHyR8%2B4GwDmZLJIUpD&winzoom=1#)

# 公众号关注 “GitHubDaily”设为 “星标”，每天带你逛 GitHub！



![img](https://mmbiz.qpic.cn/mmbiz_jpg/uDRkMWLia28iaLUoMMg5OgzcOSFIztfYDIWHEhdCXKjnB2ZazkshoBkuAITibGjx6Ueqjkic6tFAn2kia0kmLVibmwZg/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)



今天再来推荐 5 个好用的 Java 项目快速开发脚手架 / 项目骨架搭建脚手架，下面推荐的项目除了 renren 之外，其他都是我从 GitHub 上找的。

并且，我还在朋友圈调查了一波大家觉得比较好用脚手架，调查结果就在概览下面。

## 概览

1. eladmin （8.9k star）：权限管理系统。
2. renren（约 2.1k） ：Java 项目脚手架
3. SpringBlade (2.6k star) ：一个由商业级项目升级优化而来的 SpringCloud 分布式微服务架构、SpringBoot 单体式微服务架构并存的综合型项目。
4. COLA （2.1k star）：创建属于你的干净的面向对象和分层架构项目骨架。
5. SpringBoot_v2（0.7k star） ：努力打造 springboot 框架的极致细腻的脚手架。

**根据昨天我在朋友圈发起的调查来看， eladmin 、renren 、SpringBlade 好评度最高， COLA 、SpringBoot_v2 次之。**

ps：很多人推荐了 renren-fast，我感觉一般吧！手动狗头～～～有木有实际用过，并且和其他开源项目比如 eladmin 对比过的小伙伴在评论区说服一下我。

## eladmin

**推荐指数** ：⭐⭐⭐⭐⭐

### 简介

eladmin 是一款基于 Spring Boot 2.1.0 、 Jpa、 Spring Security、redis、Vue 的前后端分离的后台管理系统，项目采用分模块开发方式， 权限控制采用 RBAC，支持数据字典与数据权限管理，支持一键生成前后端代码，支持动态路由。

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTygicboQldeMtuZstSicyic1KvKb1AXNev6GuvVsUq3PwQy9465p0BAxUoQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

**相关地址** ：

1. GitHub 地址：**https://github.com/elunez/eladmin**
2. 官网：**https://docs.auauz.net/**
3. 文档：**https://docs.auauz.net/guide/**

### 推荐理由

1. 项目基本稳定，并且后续作者还会继续优化。
2. 完全开源！这个真的要为原作者点个赞，如果大家觉得这个项目有用的话，建议可以稍微捐赠一下原作者支持一下。
3. 后端整理代码质量、表设计等各个方面来说都是很不错的。
4. 前后端分离，前端使用的是国内常用的 vue 框架，比较容易上手。
5. 前端样式美观，是我这篇文章推荐的几个开源项目中前端样式最好看的一个。
6. 权限控制采用 RBAC，支持数据字典与数据权限管理。

### 项目展示

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyxHq4o0Y8BZeib9Qy6teqopT2SH0cxjlSTN6DunzlC8EBvXUGtzkibLxw/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

后台首页

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyt2xhL2aiatfujsh35vHldIia6GxVd0pGYdyUpSZicZhAaUI3GO87Z8K6Q/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

角色管理页面

## renren

**推荐指数** ：⭐⭐⭐⭐

### 简介

renren 下面一共开源了两个 Java 项目开发脚手架，分别是:

1. renren-security : 采用 Spring、MyBatis、Shiro 框架，开发的一套轻量级权限系统，极低门槛，拿来即用。
2. renren-fast : 一个轻量级的 Java 快速开发平台，能快速开发项目并交付【接私活利器】

renren-security 相比于 renren-fast 在后端功能的区别主要在于：renren-security 提供了权限管理功能，另外还额外提供了数据字典和代码生成器。

**相关地址** ：

1. renren-security ：**https://gitee.com/renrenio/renren-security**
2. renren-fast：**https://gitee.com/renrenio/renren-fast**
3. 官网：**https://www.renren.io/**

### 推荐理由

1. 被很多企业采用，说明稳定性和社区活跃度不错。
2. 微服务版 renren-cloud（这个一般企业也用不上吧！）和 renren-security 需要收费才能正常使用，renren-fast 属于完全免费并且提供了详细的文档，不过，完整文档需要捐赠 80 元才能获取到。

### 项目展示

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyEOMmfVlzcwU8JtuboH2efceqbRC9uR5YDHRRNoJMjOYAbYnRPROaDQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

renren-fast 菜单管理

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyLE4PkicgVpqOjBicNrmo07d2ne84EYGsA4aquIEXC4M8TuT8AXiaSMcyg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

renren-fast 定时任务

## SpringBlade

**推荐指数** ：⭐⭐⭐⭐⭐

### 简介

SpringBlade 是一个由商业级项目升级优化而来的 SpringCloud 分布式微服务架构、SpringBoot 单体式微服务架构并存的综合型项目，采用 Java8 API 重构了业务代码，完全遵循阿里巴巴编码规范。采用 Spring Boot 2 、Spring Cloud Hoxton 、Mybatis 等核心技术，同时提供基于 React 和 Vue 的两个前端框架用于快速搭建企业级的 SaaS 多租户微服务平台。

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyvrV8VPJQjoELEH1t7yPT7PlLNbVibLMTkCe6DUpUjQiaE5Sp0pFkIc9w/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

SpringBlade 架构图

**相关地址** ：

1. 后端 Gitee 地址：**https://gitee.com/smallc/SpringBlade**
2. 后端 GitHub 地址：**https://github.com/chillzhuang/SpringBlade**
3. 后端 SpringBoot 版：**https://gitee.com/smallc/SpringBlade/tree/2.0-boot/**
4. 前端框架 Sword (基于 React)：**https://gitee.com/smallc/Sword**
5. 前端框架 Saber (基于 Vue)：**https://gitee.com/smallc/Saber**
6. 核心框架项目地址：**https://github.com/chillzhuang/blade-tool**
7. 官网：**https://bladex.vip**

### 推荐理由

1. **允许免费用于学习、毕设、公司项目、私活等。** 如果商用的话，需要授权，并且功能更加完善。
2. 前后端分离，后端采用 SpringCloud 全家桶，单独开源出一个框架：BladeTool （感觉很厉害）
3. 集成 Sentinel 从流量控制、熔断降级、系统负载等多个维度保护服务的稳定性。
4. 借鉴 OAuth2，实现了多终端认证系统，可控制子系统的 token 权限互相隔离。
5. 借鉴 Security，封装了 Secure 模块，采用 JWT 做 Token 认证，可拓展集成 Redis 等细颗粒度控制方案。
6. 项目分包明确，规范微服务的开发模式，使包与包之间的分工清晰。

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyzjr0BL3GTUxahESIVJAo8WdPJ7tlarSiasX7RktPfFfx31aS7ibelgYQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

SpringBlade 工程结构

### 项目展示

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyHn1ylibonHey26m7YlTQqdz0h5KT0T7uRf2WQxB29A2AsosUJyzEI0g/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

Sword 后端管理页面

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyZ4eHkbCiaVyLcciaA3rdZnxLE9WaloMiaBMVINd3Sc8zeCjCXgbteqchA/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

Sword 菜单管理页面

## COLA

**推荐指数** ：⭐⭐⭐⭐⭐

### 简介

根据我的了解来看，很多公司的项目都是基于 COLA 进行开发的，相比于其他快速开发脚手架，COLA 并不提供什么已经开发好的功能，它提供的主要是一个干净的架构，然后你可以在此基础上进行开发。

如下图所示，一个通过一行命令就生成好的 web 后端项目骨架是下面这样的：

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTynoSTTQK2DGCH1QDicw6jic4ZsklbzNJNOmCXo8s4XNKy067OgCYezzhg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

COLA 应用架构

COLA 既是框架，也是架构。创建 COLA 的主要目的是为应用架构提供一套简单的可以复制、可以理解、可以落地、可以控制复杂性的” 指导和约束 "。

- 框架部分主要是以二方库的形式被应用依赖和使用。
- 架构部分主要是提供了创建符合 COLA 要求的应用 Archetype。

**相关地址**：

1. GitHub 地址：**https://github.com/alibaba/COLA**
2. COLA 2.0 介绍：**https://blog.csdn.net/significantfrank/article/details/100074716**

### 推荐理由

1. 模块之间划分清晰；
2. 一键生成项目骨架；
3. 继承了常用的类和功能比如日志功能；
4. 统一的返回格式以及错误处理；

### 项目展示

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyRnXhnJFcGNxFSvzSWgjthe6ibXoLicjcdp8p8vTEnicWm7YoET7BR5mgg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

一行命令生成的 web 后端项目骨架

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyXlvQVlghmprY67HfnYHLMiaVFS2UsNZsCJLb7u7qkXsP3JrAhBZgYNA/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

后端返回结果示意图

## SpringBoot_v2

**推荐指数** ：⭐⭐⭐⭐

### 简介

SpringBoot_v2 项目是努力打造 springboot 框架的极致细腻的脚手架。原生纯净，可在线生成 controller、mapperxml、dao、service、html、sql 代码，极大减少开发难度，增加开发进度神器脚手架！！不求回报，你使用快乐就是这个项目最大的快乐！后台管理包含代码生成器。

**相关地址** ：

1. GitHub 地址 ：**https://github.com/fuce1314/Springboot_v2**
2. Gitee 地址 ：**https://gitee.com/bdj/SpringBoot_v2**
3. 相关文档 ：**https://gitee.com/bdj/SpringBoot_v2/wikis**

### 推荐理由

1. 没有基础版、没有 vip 版本、没有付费群、没有收费二维码。
2. 对新手友好，配置好数据库连接即可运行。
3. 满足一般中小企业的基本需求。
4. 功能简单，无其他杂七杂八的功能

### 项目展示

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTyHn1ylibonHey26m7YlTQqdz0h5KT0T7uRf2WQxB29A2AsosUJyzEI0g/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

后台首页

![img](https://mmbiz.qpic.cn/mmbiz_png/iaIdQfEric9TyPxLf7muN22ZqLbx05hvTykic3GmIv8fa0Otic59v9c8AD2mV73jKgKC2Av2SqjicoYg2GFU0xhLnKg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

后台添加电子邮件