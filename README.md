# K8S运维

 **项目介绍** 

持续交付+运维

此项目用于学习，通过云平台，搭建了持续交付的流水线。

欢迎大家在此部署优秀的开源项目。

 **平台架构** 

1. 使用[GitLab]做代码仓库(维护中...)
<a href="http://gitlab.11n.wang/" target="_blank">→访问地址 </a>

1. 使用[Jenkins]实现持续交付(维护中...)
<a href="http://121.37.171.107:8080/" target="_blank">→访问地址 </a>

1. 使用[Harbor]做容器仓库(维护中...)
<a href="http://11n.wang:81" target="_blank">→访问地址 </a>

1. 使用[K8S-Kuboard]实现项目部署(维护中...)
<a href="http://121.37.184.19:32567/dashboard?k8sToken=eyJhbGciOiJSUzI1NiIsImtpZCI6InpsSU9tN0ppc0ZYZGtNMzlSMVFZR2hGa0VBNk9Oa2xGRV84RVFRVVNzU2cifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJvYXJkLXVzZXItdG9rZW4tNWhmdmQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoia3Vib2FyZC11c2VyIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiNDMyOGJmZDgtYTNhNy00ZDNmLTg4NjMtMmQwZWQ3OGMyMDQwIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmt1Ym9hcmQtdXNlciJ9.JlInICCnGU75WvrwYSDOm5hW6bDiDutC-leQrg_wWelIkXMhcopwTYkRMW1jH2M9Qya0ksHwKHER2Tus9Ia7NhMSE2XowcvikHQcYD1tZ88QLY-jK5JSU6uSRdMYKI16HNkI7OebHPHgXWgI3OA7QtLqlJxojOdvJ9zyxtuvctqKixvpLLVLoHbE4McAtacN4PMX065cojg-M7o3AFo7DCwNS0a7RkGQ4qlV1paPWgKC6vvgSLl9jy2fedc8qUVRxpj3XCewfNRemhDylXC5uZ6hy_7Fiv_Piov1utnv2D0Im2PvkLQVq5LdNZ6_6qZPZZp_LKukAGW77vxtnS3bwA" target="_blank">→访问地址 </a>

1. 使用[ZABBIX]实现平台监控
<a href="http://47.103.211.39:8080/zabbix/index.php" target="_blank">→访问地址 </a>

平台网址：

1.  代码仓库→[GitLab](http://gitlab.11n.wang/)

    用户名：root
    密码：1qaz@WSX

![输入图片说明](https://images.gitee.com/uploads/images/2020/0326/093953_3941d982_5368921.jpeg "gitlab - 副本 - 副本.jpg")

2.  持续交付→[Jenkins](http://121.37.171.107:8080/)

    用户名：admin
    密码：admin

![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/095043_bf260356_5368921.jpeg "jenkins.jpg")

3.  容器仓库→[Harbor](http://11n.wang:81)

    用户名：admin
    密码：Harbor12345

![输入图片说明](https://images.gitee.com/uploads/images/2020/0326/094015_eb495154_5368921.jpeg "harbor - 副本.jpg")

4.  项目部署→[K8S-Kuboard](http://121.37.184.19:32567/dashboard?k8sToken=eyJhbGciOiJSUzI1NiIsImtpZCI6InpsSU9tN0ppc0ZYZGtNMzlSMVFZR2hGa0VBNk9Oa2xGRV84RVFRVVNzU2cifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJvYXJkLXVzZXItdG9rZW4tNWhmdmQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoia3Vib2FyZC11c2VyIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiNDMyOGJmZDgtYTNhNy00ZDNmLTg4NjMtMmQwZWQ3OGMyMDQwIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmt1Ym9hcmQtdXNlciJ9.JlInICCnGU75WvrwYSDOm5hW6bDiDutC-leQrg_wWelIkXMhcopwTYkRMW1jH2M9Qya0ksHwKHER2Tus9Ia7NhMSE2XowcvikHQcYD1tZ88QLY-jK5JSU6uSRdMYKI16HNkI7OebHPHgXWgI3OA7QtLqlJxojOdvJ9zyxtuvctqKixvpLLVLoHbE4McAtacN4PMX065cojg-M7o3AFo7DCwNS0a7RkGQ4qlV1paPWgKC6vvgSLl9jy2fedc8qUVRxpj3XCewfNRemhDylXC5uZ6hy_7Fiv_Piov1utnv2D0Im2PvkLQVq5LdNZ6_6qZPZZp_LKukAGW77vxtnS3bwA)

    免密登陆

![输入图片说明](https://images.gitee.com/uploads/images/2020/0326/093310_92550253_5368921.jpeg "k8s1.6 - 副本.jpeg")

5.  平台监控→[ZABBIX](http://47.103.211.39:8080/zabbix/index.php)

    用户名：Admin
    密码：zabbix

![输入图片说明](https://images.gitee.com/uploads/images/2020/0326/093220_a8179ab0_5368921.png "zabbix - 副本.png")

#### 使用说明

简易的命令行入门教程:

Git 全局设置:

git config --global user.name "夜微凉"

git config --global user.email "1132808592@qq.com"

创建 git 仓库:

mkdir test

cd test

git init

touch doc.md

git add doc.md

git commit -m "first commit"

git remote add origin https://gitee.com/TzWind/k8sadmin.git

git push -u origin master


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

